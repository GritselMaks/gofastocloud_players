package main

import (
	"flag"

	log "github.com/sirupsen/logrus"

	"gitlab.com/fastogt/gofastocloud_players/app"
)

func main() {
	configPath := flag.String("config", "/etc/players.conf", "service config")
	flag.Parse()
	cfg, err := app.LoadConfig(*configPath)
	if err != nil {
		log.Fatal("App::Initialize load config error: ", err)
	}
	s := app.AppServer{}
	s.Initialize(*cfg)
	s.Run()

}
