package app

import (
	"encoding/json"
	"net/http"

	"gitlab.com/fastogt/gofastocloud_models/constans"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/ott"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (app *AppServer) Login() http.HandlerFunc {
	type request struct {
		IDDevice string `json:"id"`
	}
	type response struct {
		Access_token string `json:"access_token"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			respondWithError(w, http.StatusBadRequest, ErrNoBasicAuth)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(user)
		if err != nil && err != mongo.ErrNoDocuments {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		if err == mongo.ErrNoDocuments {
			respondWithError(w, http.StatusBadRequest, ErrSubscriberNotSingUp)
			return
		}
		if !CompareHashMD5(subscriber.Password, pass) {
			respondWithError(w, http.StatusBadRequest, ErrWrongPassword)
			return
		}
		if !ControlExperedTime(subscriber.ExpDate) {
			respondWithError(w, http.StatusBadRequest, ErrExpiredDate)
			return
		}
		var req request
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		if len(req.IDDevice) == 0 {
			respondWithError(w, http.StatusBadRequest, ErrRequiredFields)
			return
		}
		id, err := primitive.ObjectIDFromHex(req.IDDevice)
		if err != nil {
			respondWithError(w, http.StatusOK, ErrBadRequestData)
			return
		}
		var token *string
		for i, device := range subscriber.Devices {
			if device.IDDevice == id {
				if device.Status == front.BANNED_DEVICE {
					respondWithError(w, http.StatusBadRequest, ErrDeviceBaned)
					return
				}
				if device.Status != front.ACTIVE_DEVICE {
					subscriber.Devices[i].Status = front.ACTIVE_DEVICE
					if err := app.database.SubscriberCollection.UpdateDevices(subscriber.ID, subscriber.Devices); err != nil {
						respondWithError(w, http.StatusOK, ErrBadRequestData)
						return
					}
				}
				token, err = GenerateJWT(user, req.IDDevice)
				if err != nil {
					respondWithError(w, http.StatusBadRequest, ErrGenerateToken)
					return
				}
				break
			}
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{Access_token: *token}))
	}
}

func (app *AppServer) GetDevices() http.HandlerFunc {
	type response struct {
		Devices []front.DevicesFront `json:"devices"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			respondWithError(w, http.StatusBadRequest, ErrNoBasicAuth)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(user)
		if err != nil && err != mongo.ErrNoDocuments {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		if err == mongo.ErrNoDocuments {
			respondWithError(w, http.StatusBadRequest, ErrSubscriberNotSingUp)
			return
		}
		if !CompareHashMD5(subscriber.Password, pass) {
			respondWithError(w, http.StatusBadRequest, ErrWrongPassword)
			return
		}
		if !ControlExperedTime(subscriber.ExpDate) {
			respondWithError(w, http.StatusBadRequest, ErrExpiredDate)
			return
		}
		if subscriber.Devices == nil {
			respondWithError(w, http.StatusBadRequest, ErrExpiredDate)
			return
		}
		devices := []front.DevicesFront{}
		for _, d := range subscriber.Devices {
			devices = append(devices, *d.ToFront())
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{Devices: devices}))
	}
}

func (app *AppServer) AddDevice() http.HandlerFunc {
	type request struct {
		DeviceName string `json:"device_name"`
	}
	type response struct {
		IDDev string `json:"id"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			respondWithError(w, http.StatusBadRequest, ErrNoBasicAuth)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(user)
		if err != nil && err != mongo.ErrNoDocuments {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		if err == mongo.ErrNoDocuments {
			respondWithError(w, http.StatusBadRequest, ErrSubscriberNotSingUp)
			return
		}
		if !CompareHashMD5(subscriber.Password, pass) {
			respondWithError(w, http.StatusBadRequest, ErrWrongPassword)
			return
		}
		if !ControlExperedTime(subscriber.ExpDate) {
			respondWithError(w, http.StatusBadRequest, ErrExpiredDate)
			return
		}
		if len(subscriber.Devices) >= subscriber.MaxDeviceCount {
			respondWithError(w, http.StatusBadRequest, ErrMaxCountDevices)
			return
		}
		var req request
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		newDevice := ott.NewDevice()
		if len(req.DeviceName) != 0 {
			newDevice.Name = req.DeviceName
		}
		if err = app.database.SubscriberCollection.AddDevice(subscriber.ID, *newDevice); err != nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{IDDev: newDevice.IDDevice.Hex()}))
	}
}

func (app *AppServer) GetStreams() http.HandlerFunc {
	type response struct {
		Streams []player.ChannelInfo `json:"streams"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		email, err := JWTAuth(r)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(*email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		streams := []player.ChannelInfo{}
		var channelInfo *player.ChannelInfo
		for _, ustream := range subscriber.Streams {
			mongoResult, err := app.database.StreamCollection.GetStreamByID(&ustream.IDStream)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
			channelInfo, err = app.getStreamInfo(mongoResult, &ustream)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
			streams = append(streams, *channelInfo)
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{Streams: streams}))
	}
}

func (app *AppServer) GetVods() http.HandlerFunc {
	type response struct {
		Vods []player.VodInfo `json:"vods"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		email, err := JWTAuth(r)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(*email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		vods := []player.VodInfo{}
		for _, ustream := range subscriber.Vods {
			mongoResult, err := app.database.StreamCollection.GetStreamByID(&ustream.IDStream)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
			vodInfo, err := app.getVodInfo(mongoResult, &ustream)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
			vods = append(vods, *vodInfo)
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{Vods: vods}))
	}
}

func (app *AppServer) GetCatchups() http.HandlerFunc {
	type response struct {
		Catchups []player.CatchupInfo `json:"cathcups"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		email, err := JWTAuth(r)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(*email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		catchups := []player.CatchupInfo{}
		for _, ustream := range subscriber.Catchups {
			mongoResult, err := app.database.StreamCollection.GetStreamByID(&ustream.IDStream)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
			catchupInfo, err := app.getCatchupInfo(mongoResult, &ustream)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
			catchups = append(catchups, *catchupInfo)
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{Catchups: catchups}))
	}
}

func (app *AppServer) GetSeries() http.HandlerFunc {
	type response struct {
		Series []front.SerialFront `json:"series"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		email, err := JWTAuth(r)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		subscriberSeries, err := app.database.SubscriberCollection.GetSubscriberSeries(*email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		series, err := app.database.SeriesCollection.GetListSerialByID(subscriberSeries)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}

		var seriesFront []front.SerialFront
		for _, seria := range series {
			seriesFront = append(seriesFront, *gofastocloud_models.MakeSerialToFront(&seria))
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{Series: seriesFront}))
	}
}

func (app *AppServer) GetServerInfo() http.HandlerFunc {
	type response struct {
		Epg        string `json:"epg_url"`
		LockedText string `json:"locked_text"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		if _, err := JWTAuth(r); err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(response{Epg: app.config.Epg, LockedText: app.config.LockedText}))
	}
}

func (app *AppServer) SetFavorite() http.HandlerFunc {
	type request struct {
		IDStream *string `json:"id"`
		Favorite *bool   `json:"favorite"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		email, err := JWTAuth(r)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		var req request
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		if req.Favorite == nil || req.IDStream == nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		idStream, err := primitive.ObjectIDFromHex(*req.IDStream)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(*email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		typeStream, err := app.database.StreamCollection.GetTypeStreamByID(&idStream)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, ErrStreamNotFound)
			return
		}
		switch *typeStream {
		case constans.VOD_RELAY_STR, constans.VOD_ENCODE_STR, constans.VOD_PROXY_STR:
			err = app.database.SubscriberCollection.SetFavoriteVods(subscriber.ID, idStream, *req.Favorite)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		case constans.CATCHUP_STR:
			err = app.database.SubscriberCollection.SetFavoriteCathcups(subscriber.ID, idStream, *req.Favorite)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		default:
			err = app.database.SubscriberCollection.SetFavoriteStreams(subscriber.ID, idStream, *req.Favorite)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		}
		respondWithOk(w)
	}
}

func (app *AppServer) SetRecent() http.HandlerFunc {
	type request struct {
		IDStream *string                `json:"id"`
		Recent   *gofastogt.UtcTimeMsec `json:"recent"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		email, err := JWTAuth(r)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		var req request
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		if req.Recent == nil || req.IDStream == nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		idStream, err := primitive.ObjectIDFromHex(*req.IDStream)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(*email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		typeStream, err := app.database.StreamCollection.GetTypeStreamByID(&idStream)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, ErrStreamNotFound)
			return
		}
		switch *typeStream {
		case constans.VOD_RELAY_STR, constans.VOD_ENCODE_STR, constans.VOD_PROXY_STR:
			err = app.database.SubscriberCollection.SetRecentVods(subscriber.ID, idStream, gofastogt.UtcTime2Time(*req.Recent))
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		case constans.CATCHUP_STR:
			err = app.database.SubscriberCollection.SetRecentCathcups(subscriber.ID, idStream, gofastogt.UtcTime2Time(*req.Recent))
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		default:
			err = app.database.SubscriberCollection.SetRecentStreams(subscriber.ID, idStream, gofastogt.UtcTime2Time(*req.Recent))
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		}
		respondWithOk(w)
	}
}

func (app *AppServer) SetInterruptTime() http.HandlerFunc {
	type request struct {
		IDStream     *string `json:"id"`
		Interruption *int64  `json:"interruption_time"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		email, err := JWTAuth(r)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err)
			return
		}
		var req request
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		if req.Interruption == nil || req.IDStream == nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		idStream, err := primitive.ObjectIDFromHex(*req.IDStream)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, ErrBadRequestData)
			return
		}
		subscriber, err := app.database.SubscriberCollection.FindByEmail(*email)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err)
			return
		}
		typeStream, err := app.database.StreamCollection.GetTypeStreamByID(&idStream)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, ErrStreamNotFound)
			return
		}
		switch *typeStream {
		case constans.VOD_RELAY_STR, constans.VOD_ENCODE_STR, constans.VOD_PROXY_STR:
			err = app.database.SubscriberCollection.SetInterruptionVods(subscriber.ID, idStream, *req.Interruption)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		case constans.CATCHUP_STR:
			err = app.database.SubscriberCollection.SetInterruptionCathcups(subscriber.ID, idStream, *req.Interruption)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		default:
			err = app.database.SubscriberCollection.SetInterruptionStreams(subscriber.ID, idStream, *req.Interruption)
			if err != nil {
				respondWithError(w, http.StatusBadRequest, err)
				return
			}
		}
		respondWithOk(w)
	}
}

func (app *AppServer) getStreamInfo(mongoResult *mongo.SingleResult, ustream *gofastocloud_models.UserStream) (*player.ChannelInfo, error) {
	var istream gofastocloud_models.IStream
	if err := mongoResult.Decode(&istream); err != nil {
		return nil, ErrGetChannelInfo
	}
	switch istream.TypeStream {
	case constans.PROXY_STR:
		video := true
		audio := true
		return gofastocloud_models.MakeChannelInfo(&istream, ustream, &video, &audio), nil
	case constans.RELAY_STR:
		var relayStream gofastocloud_models.RelayStream
		if err := mongoResult.Decode(&relayStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeChannelInfo(&relayStream.IStream, ustream, &relayStream.HaveVideo, &relayStream.HaveAudio), nil
	case constans.ENCODE_STR:
		var encodeStream gofastocloud_models.EncodeStream
		if err := mongoResult.Decode(&encodeStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeChannelInfo(&encodeStream.IStream, ustream, &encodeStream.HaveVideo, &encodeStream.HaveAudio), nil
	case constans.TIMESHIFT_PLAYER_STR:
		var timeshiftPlayerStream gofastocloud_models.TimeshiftPlayerStream
		if err := mongoResult.Decode(&timeshiftPlayerStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeChannelInfo(&timeshiftPlayerStream.IStream, ustream, &timeshiftPlayerStream.HaveVideo, &timeshiftPlayerStream.HaveAudio), nil
	case constans.COD_RELAY_STR:
		var codRelayStream gofastocloud_models.CodRelayStream
		if err := mongoResult.Decode(&codRelayStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeChannelInfo(&codRelayStream.IStream, ustream, &codRelayStream.HaveVideo, &codRelayStream.HaveAudio), nil
	case constans.COD_ENCODE_STR:
		var codEncodeStream gofastocloud_models.CodEncodeStream
		if err := mongoResult.Decode(&codEncodeStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeChannelInfo(&codEncodeStream.IStream, ustream, &codEncodeStream.HaveVideo, &codEncodeStream.HaveAudio), nil
	}
	return nil, ErrGetChannelInfo
}

func (app *AppServer) getVodInfo(mongoResult *mongo.SingleResult, ustream *gofastocloud_models.UserStream) (*player.VodInfo, error) {
	var istream gofastocloud_models.IStream
	if err := mongoResult.Decode(&istream); err != nil {
		return nil, ErrGetChannelInfo
	}
	switch istream.TypeStream {

	case constans.VOD_PROXY_STR:
		video := true
		audio := true
		var vodProxyStream gofastocloud_models.VodProxyStream
		if err := mongoResult.Decode(&vodProxyStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeVodInfo(&vodProxyStream.IStream, &vodProxyStream.VodBasedStream, ustream, &video, &audio), nil
	case constans.VOD_RELAY_STR:
		var vodRelayStream gofastocloud_models.VodRelayStream
		if err := mongoResult.Decode(&vodRelayStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeVodInfo(&vodRelayStream.IStream, &vodRelayStream.VodBasedStream, ustream, &vodRelayStream.HaveVideo, &vodRelayStream.HaveAudio), nil
	case constans.VOD_ENCODE_STR:
		var vodEncodeStream gofastocloud_models.VodEncodeStream
		if err := mongoResult.Decode(&vodEncodeStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeVodInfo(&vodEncodeStream.IStream, &vodEncodeStream.VodBasedStream, ustream, &vodEncodeStream.HaveVideo, &vodEncodeStream.HaveAudio), nil
	}
	return nil, ErrGetChannelInfo
}

func (app *AppServer) getCatchupInfo(mongoResult *mongo.SingleResult, ustream *gofastocloud_models.UserStream) (*player.CatchupInfo, error) {
	var istream gofastocloud_models.IStream
	if err := mongoResult.Decode(&istream); err != nil {
		return nil, ErrGetChannelInfo
	}
	if istream.TypeStream == constans.CATCHUP_STR {
		var catchupsStream gofastocloud_models.CatchupStream
		if err := mongoResult.Decode(&catchupsStream); err != nil {
			return nil, err
		}
		return gofastocloud_models.MakeCatchupInfo(&catchupsStream, ustream, &catchupsStream.HaveVideo, &catchupsStream.HaveAudio), nil
	}
	return nil, ErrGetChannelInfo
}
