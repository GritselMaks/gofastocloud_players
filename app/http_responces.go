package app

import (
	"encoding/json"
	"net/http"

	"gitlab.com/fastogt/gofastogt"
)

func respondWithStructErrorJSON(w http.ResponseWriter, statusCode int, resp *gofastogt.ErrorResponse) {
	response, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(response)
}

func respondWithError(w http.ResponseWriter, statusCode int, err error) {
	resp := gofastogt.ErrorJson{Code: statusCode, Message: err.Error()}
	payload := gofastogt.NewErrorResponse(resp)
	respondWithStructErrorJSON(w, statusCode, payload)
}

func respondWithStructJSON(w http.ResponseWriter, statusCode int, payload *gofastogt.OkResponse) {
	response, err := json.Marshal(payload)
	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(response)
}

func respondWithOk(w http.ResponseWriter) {
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(nil))
}
