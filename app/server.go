package app

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud_players/app/store"
	"gitlab.com/fastogt/gofastogt"
)

type AppServer struct {
	config   Config
	http     *http.Server
	logFile  *os.File
	database store.MongoDataBase
}

func (app *AppServer) Initialize(config Config) {
	app.config = config

	logFilePath, err := gofastogt.StableFilePath(config.LogPath)
	if err != nil {
		log.Error(err)
	}
	app.logFile, err = os.OpenFile(*logFilePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Error(err)
	} else {
		log.SetOutput(app.logFile)
	}
	app.configLogger()

	err = app.database.InitializeMongoDB(app.config.DBUrl)
	if err != nil {
		log.Error("error initialize database")
	}

	app.http = app.configServerRoutes()
	log.Info("Server initialised")
}
func (app *AppServer) Run() {
	log.Info("Server started")
	if err := app.http.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Error(err)
	}
	log.Info("Server finished")
}

func (app *AppServer) configLogger() {
	level, err := log.ParseLevel(app.config.LogLevel)
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)
}

func (app *AppServer) configServerRoutes() *http.Server {
	router := mux.NewRouter()
	//Basic Auth routes
	router.HandleFunc("/client/devices", app.GetDevices()).Methods("GET")
	router.HandleFunc("/client/devices/add", app.AddDevice()).Methods("POST")
	router.HandleFunc("/client/login", app.Login()).Methods("POST")

	//JWT token routes
	router.HandleFunc("/client/streams", app.GetStreams()).Methods("GET")
	router.HandleFunc("/client/vods", app.GetVods()).Methods("GET")
	router.HandleFunc("/client/catchups", app.GetCatchups()).Methods("GET")
	router.HandleFunc("/client/series", app.GetSeries()).Methods("GET")
	router.HandleFunc("/client/server_info", app.GetServerInfo()).Methods("GET")
	router.HandleFunc("/client/set_favorite", app.SetFavorite()).Methods("POST")
	router.HandleFunc("/client/set_recent", app.SetRecent()).Methods("POST")
	router.HandleFunc("/client/set_interruption_time", app.SetInterruptTime()).Methods("POST")

	return NewHttpServer(app.config.Host, app.config.Cors, router)
}
