package store

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type StreamCollection struct {
	handle *mongo.Collection
}

func NewStreamCollection(handle *mongo.Collection) *StreamCollection {
	return &StreamCollection{handle: handle}
}

func (collection StreamCollection) GetTypeStreamByID(id *primitive.ObjectID) (*string, error) {
	if collection.handle == nil {
		return nil, errors.New("invalid input")
	}
	type singleResult struct {
		Type *string `bson:"_cls"`
	}
	var result singleResult
	filter := bson.M{"_id": id}
	opts := options.FindOne().SetProjection(bson.D{{Key: "_cls", Value: 1}, {Key: "_id", Value: 0}})
	err := collection.handle.FindOne(context.TODO(), filter, opts).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result.Type, nil
}

func (collection StreamCollection) GetStreamByID(id *primitive.ObjectID) (*mongo.SingleResult, error) {
	if collection.handle == nil {
		return nil, errors.New("invalid input")
	}
	res := collection.handle.FindOne(context.TODO(), bson.M{"_id": id})
	if err := res.Err(); err != nil {
		return nil, err
	}
	return res, nil
}
