package store

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDataBase struct {
	SubscriberCollection *SubscribersCollection
	StreamCollection     *StreamCollection
	SeriesCollection     *SeriesCollection
}

func (db *MongoDataBase) InitializeMongoDB(uri string) (err error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return err
	}

	err = client.Connect(context.TODO())
	if err != nil {
		return err
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return err
	}
	dataBase := client.Database("iptv")
	db.SubscriberCollection = NewSubscribersCollection(dataBase.Collection("subscribers"))
	db.StreamCollection = NewStreamCollection(dataBase.Collection("streams"))
	db.SeriesCollection = NewSeriesCollection(dataBase.Collection("series"))
	return nil

}
