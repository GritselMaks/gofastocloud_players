package store

import (
	"context"
	"errors"
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/ott"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type SubscribersCollection struct {
	handle *mongo.Collection
}

func NewSubscribersCollection(handle *mongo.Collection) *SubscribersCollection {
	return &SubscribersCollection{handle: handle}
}

func (collection *SubscribersCollection) FindByEmail(email string) (*gofastocloud_models.Subscriber, error) {
	if collection.handle == nil {
		return nil, errors.New("invalid input")
	}
	var subscriber gofastocloud_models.Subscriber
	filter := bson.M{"email": email}
	err := collection.handle.FindOne(context.TODO(), filter).Decode(&subscriber)
	if err != nil {
		return nil, err
	}
	return &subscriber, nil
}

func (collection *SubscribersCollection) GetSubscriberSeries(email string) ([]primitive.ObjectID, error) {
	if collection.handle == nil {
		return nil, errors.New("invalid input")
	}
	type singleResult struct {
		Series []primitive.ObjectID `bson:"series"`
	}
	var result singleResult
	filter := bson.M{"email": email}
	opts := options.FindOne().SetProjection(bson.D{{Key: "series", Value: 1}, {Key: "_id", Value: 0}})
	err := collection.handle.FindOne(context.TODO(), filter, opts).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result.Series, nil
}

func (collection *SubscribersCollection) UpdateDevices(id primitive.ObjectID, devices []ott.Device) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": id}
	update := bson.M{
		"$set": bson.M{
			"devices": devices,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) AddDevice(id primitive.ObjectID, device ott.Device) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": id}
	update := bson.M{"$push": bson.M{"devices": device}}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetFavoriteStreams(idSubscriber, idStream primitive.ObjectID, favorite bool) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "streams.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"streams.$.favorite": favorite,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetFavoriteVods(idSubscriber, idStream primitive.ObjectID, favorite bool) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "vods.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"vods.$.favorite": favorite,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetFavoriteCathcups(idSubscriber, idStream primitive.ObjectID, favorite bool) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "catchups.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"catchups.$.favorite": favorite,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetRecentStreams(idSubscriber, idStream primitive.ObjectID, recent time.Time) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "streams.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"streams.$.recent": recent,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetRecentVods(idSubscriber, idStream primitive.ObjectID, recent time.Time) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "vods.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"vods.$.recent": recent,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetRecentCathcups(idSubscriber, idStream primitive.ObjectID, recent time.Time) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "catchups.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"catchups.$.recent": recent,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetInterruptionStreams(idSubscriber, idStream primitive.ObjectID, interruption int64) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "streams.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"streams.$.interruption_time": interruption,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetInterruptionVods(idSubscriber, idStream primitive.ObjectID, interruption int64) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "vods.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"vods.$.interruption_time": interruption,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (collection *SubscribersCollection) SetInterruptionCathcups(idSubscriber, idStream primitive.ObjectID, interruption int64) error {
	if collection.handle == nil {
		return errors.New("invalid input")
	}
	filter := bson.M{"_id": idSubscriber, "catchups.sid": idStream}
	update := bson.M{
		"$set": bson.M{
			"catchups.$.interruption_time": interruption,
		},
	}
	_, err := collection.handle.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}
	return nil
}
