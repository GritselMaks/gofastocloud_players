package store

import (
	"context"
	"errors"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type SeriesCollection struct {
	handle *mongo.Collection
}

func NewSeriesCollection(handle *mongo.Collection) *SeriesCollection {
	return &SeriesCollection{handle: handle}
}

func (collection SeriesCollection) GetListSerialByID(id []primitive.ObjectID) ([]gofastocloud_models.Serial, error) {
	if collection.handle == nil {
		return nil, errors.New("invalid input")
	}
	var serials []gofastocloud_models.Serial
	var s gofastocloud_models.Serial
	for _, idSerial := range id {
		filter := bson.M{"_id": idSerial}
		err := collection.handle.FindOne(context.TODO(), filter).Decode(&s)
		if err != nil {
			return nil, err
		}
		serials = append(serials, s)
	}
	return serials, nil
}
