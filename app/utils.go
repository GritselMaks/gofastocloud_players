package app

import (
	"crypto/md5"
	"encoding/hex"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go/v4"
	"github.com/rs/cors"
)

const SECRET_KEY string = "pass"

func GetHashMD5(s string) string {
	hash := md5.Sum([]byte(s))
	return hex.EncodeToString(hash[:])
}

func CompareHashMD5(hasdedStr, compareStr string) bool {
	comPass := GetHashMD5(compareStr)
	return hasdedStr == comPass
}
func ControlExperedTime(expiredDate time.Time) bool {
	return expiredDate.After(time.Now())
}

// Generate JWT token
func GenerateJWT(email, id string) (*string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = email
	claims["idDevice"] = id
	tokenString, err := token.SignedString([]byte(SECRET_KEY))
	if err != nil {
		return nil, err
	}
	return &tokenString, nil
}

// Validation JWT token.
// Function receive http.Request, return payload from token and error
func JWTAuth(r *http.Request) (*string, error) {
	tokenHeader := r.Header.Get("Authorization")
	if len(tokenHeader) == 0 {
		return nil, ErrNoAuthToken
	}
	splitted := strings.Split(tokenHeader, " ")
	if len(splitted) != 2 {
		return nil, ErrBadFormatToken
	}
	tokenHeader = splitted[1]
	token, err := jwt.ParseWithClaims(tokenHeader, jwt.MapClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SECRET_KEY), nil
	})
	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok {
		email := claims["email"].(string)
		return &email, nil
	}
	return nil, nil
}

func NewHttpServer(addr string, corsEnabled bool, handler http.Handler) *http.Server {
	if corsEnabled {
		options := cors.Options{
			AllowedOrigins:   []string{"*"},
			AllowedHeaders:   []string{"*"},
			AllowCredentials: true,
		}
		c := cors.New(options)
		corsHandler := c.Handler(handler)
		return &http.Server{Addr: addr, Handler: corsHandler}
	}

	return &http.Server{Addr: addr, Handler: handler}
}
