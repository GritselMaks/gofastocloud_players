package app

import "errors"

var ErrSubscriberNotSingUp = errors.New("email doesn't signuped")
var ErrRequiredFields = errors.New("required fields cant be empty")
var ErrWrongPassword = errors.New("wrong password")
var ErrExpiredDate = errors.New("subscriber is expired")
var ErrGenerateToken = errors.New("error generate jwttoken")

var ErrNoBasicAuth = errors.New("error parsing basic auth")
var ErrNoAuthToken = errors.New("not auth token")
var ErrBadFormatToken = errors.New("invalid format token, use (Bearer <token>)")

var ErrBadRequestData = errors.New("bad request data")
var ErrMaxCountDevices = errors.New("max count activated devices")
var ErrDeviceNotFound = errors.New("device not found")
var ErrDeviceBaned = errors.New("device baned")
var ErrStreamNotFound = errors.New("stream not found")
var ErrGetChannelInfo = errors.New("error Get Channel Info")
