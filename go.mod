module gitlab.com/fastogt/gofastocloud_players

go 1.13

require (
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.8.2
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/fastogt/gofastocloud_models v0.2.12
	gitlab.com/fastogt/gofastogt v1.2.1
	go.mongodb.org/mongo-driver v1.9.0
	gopkg.in/yaml.v2 v2.4.0
)
