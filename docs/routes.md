Service for connection subscribers devices.

Service have there are methods with basic auth(username:password):
* Get list subscriber devices
* Add devices to subscriber
* Login subscriber

## API

|Method         | Path           | Request JSON    | Responce JSON   |
| :-----------: | :------------: | :-------------: | :----------------: | 
| GET           | /client/devices     | | {"devices": []{"id": string,"name": string,"status": int,"created_date": int}} |
| POST          | /client/devices/add | {"device_name": "string"} | {"id":"string"} |
| POST          | /client/login       | {"id":"string"} | {"access_token": "string"} |

Notes:
1. Fields in request is required
2. Field "id" is the 12-byte ObjectId value in string format.


Other metods use JWT token:
* Get list streams
* Get list vods
* Get list catchups
* Get list series
* Get server info
* Set favorite stream
* Set recent time
* Set interruption time

## API

|Method         | Path             | Request JSON     | Responce JSON   |
| :-----------: | :------------:   | :-------------:  | :----------------: | 
| GET           | /client/streams  |           | {"data":{"streams":[] {"id":string, "groups":[]string, "iarc":int,"favorite":bool,"locked":bool,"recent":int64,"interruption_time":int64, "epg":object{"id":string,"urls":[]string, "display_name":string,"icon":string,"programs":[]object}, "video":bool,"audio":bool,"parts":[]string, "view_count":int,"meta":[]object, "created_date":int64,"archive":bool}}}  |
| GET           | /client/vods     |           | {"data":{"vods": []{"id":string, "groups":[]string, "iarc":int,"favorite":bool,"locked":bool,"recent":int64,"interruption_time":int64,"video":bool,"audio":bool,"parts":[]string, "view_count":int,"meta":[]object,"created_date":int64,"vod": object{"urls":[]string,"description":string,"display_name":string,"backgorund_url":string,"preview_icon":string,"trailer_url":string,"user_score":int,"prime_date":int,"country":string,"duration":int64,"vod_type":int}}}   |
| GET           | /client/catchups |           | {"data":{"catchups": []{"id":string, "groups":[]string, "iarc":int,"favorite":bool,"locked":bool,"recent":int64,"interruption_time":int64, "epg":object{"id":string,"urls":[]string, "display_name":string,"icon":string,"programs":[]object}, "video":bool,"audio":bool,"parts":[]string, "view_count":int,"meta":[]object, "created_date":int64,"archive":bool, "start":int,"stop":int}}} |
| GET           | /client/series        |           | { "data":{"series": []{"id":string,"name":string, "background":string, "icon":string, "groups":[]string, "description":string, "season":int, "episodes":[]string,"price":float,"view_count":int,"createdDate":int64}}}   |
| GET           | /client/server_info   |           | { "data":{"epg_url": string, "locked_text": string}}   |
| POST          | /client/set_favorite  | {"id": string, "favorite":bool} | {"data": null}  |
| POST          | /client/set_recent    | {"id": string, "recent":int}  | {"data": null}  |
| POST          | /client/set_interruption_time    | {"id": string, "interruption_time":int}  | {"data": null}  |

Notes:
1. Fields in request is required
2. Field "id" is the 12-byte ObjectId value in string format.
3. Fields "recent" and "interruption_time" it's time in milliseconds